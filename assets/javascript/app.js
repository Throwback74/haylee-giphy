
var topics = ["Batman", "Wonder Woman", "Shazam", " Black Canary"];
var topic = '';

function renderButtons() {
  $("#buttons-go-here").empty();
  $('#heroes-input').empty();
  for (var i = 0; i < topics.length; i++) {
    var a = $("<button>");
    a.addClass("superhero-btn");
    a.attr("data-person", topics[i]);
    a.text(topics[i]);
    $("#buttons-go-here").append(a);
  };
};

$('#addHero').on('click', function(event) {
  event.preventDefault();
  topic = $('#heroes-input').val().trim();
  if(topics.indexOf(topic) === -1) {
    topics.push(topic)
  } else {
    console.log("This Item already exists")
  }
  renderButtons();
});

var ajaxCall = function () {
  // $("button").on("click", function() { 
    var person = $(this).attr("data-person");
    var queryURL = "https://api.giphy.com/v1/gifs/search?q=" +
      person + "&api_key=yC00khS66NHHHHxyC4azZPgAS16agWqJ&limit=10";

    $.ajax({
      url: queryURL,
      method: "GET"
    })
      .then(function(response) {
        var results = response.data;
        for (var i = 0; i < results.length; i++) {
          var superDiv = $("<div>");
          var p = $("<p>").text("Rating: " + results[i].rating);
          var personImage = $("<img>");
          personImage.attr("src", results[i].images.fixed_height.url);
          superDiv.append(p);
          superDiv.append(personImage);
          $("#gifs-appear-here").prepend(superDiv);
        }
      });
  };

  $(document).on("click", ".superhero-btn", ajaxCall);

  renderButtons();
